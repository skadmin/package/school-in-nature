<?php

declare(strict_types=1);

namespace Skadmin\SchoolInNature\Components\Admin;

use App\Model\Doctrine\User\UserFacade;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\Utils\Html;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Container;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Nette\Utils\Strings;
use Skadmin\SchoolInNature\BaseControl;
use Skadmin\SchoolInNature\Doctrine\SchoolInNature\SchoolInNature;
use Skadmin\SchoolInNature\Doctrine\Staff\Staff as eStaff;
use Skadmin\SchoolInNature\Doctrine\Staff\StaffFacade;
use Skadmin\Translator\Translator;
use Ublaboo\DataGrid\Column\Action\Confirmation\StringConfirmation;
use function intval;
use function sprintf;

class Staff extends GridControl
{
    use APackageControl;

    private StaffFacade    $facade;
    private UserFacade     $facadeUser;
    private SchoolInNature $schoolInNature;
    private ?GridDoctrine  $grid = null;

    public function __construct(SchoolInNature $schoolInNature, StaffFacade $facade, UserFacade $facadeUser, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);
        $this->facadeUser = $facadeUser;

        $this->schoolInNature = $schoolInNature;
        $this->facade         = $facade;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/staff.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'school-in-nature.staff.title';
    }

    public function gridInlineEditCreate(Container $container): void
    {
        $container->addText('name', 'grid.school-in-nature.staff.name');
        $container->addText('description', 'grid.school-in-nature.staff.description');
        $container->addText('number', 'grid.school-in-nature.staff.number');
        $container->addText('financial_reward', 'grid.school-in-nature.staff.financial-reward');
        $container->addText('financial_premium', 'grid.school-in-nature.staff.financial-premium');
    }

    public function gridInlineEditDefaults(Container $container, eStaff $staff): void
    {
        $container->setDefaults([
            'name'              => $staff->getName(),
            'description'       => $staff->getDescription(),
            'number'            => $staff->getNumber(),
            'financial_reward'  => $staff->getFinancialReward(),
            'financial_premium' => $staff->getFinancialPremium(),
        ]);
    }

    public function gridInlineEditSubmit(string $id, ArrayHash $values): void
    {
        $name = Strings::trim($values->name);

        if ($name !== '') {
            $staff = $this->facade->update(
                intval($id),
                $this->schoolInNature,
                $name,
                $values->description,
                intval($values->number),
                intval($values->financial_reward),
                $values->financial_premium
            );

            $this->grid->redrawItem($id);
            $this->getParent()->redrawControl('snipStaffCount');
        }
    }

    public function gridInlineAddCreate(Container $container): void
    {
        $container->addText('name', 'grid.school-in-nature.staff.name');
        $container->addText('description', 'grid.school-in-nature.staff.description');
        $container->addText('number', 'grid.school-in-nature.staff.number');
        $container->addText('financial_reward', 'grid.school-in-nature.staff.financial-reward');
        $container->addText('financial_premium', 'grid.school-in-nature.staff.financial-premium');
    }

    public function gridInlineAddSubmit(ArrayHash $values): void
    {
        $name = Strings::trim($values->name);

        if ($name !== '') {
            $this->facade->create($this->schoolInNature, $name, $values->description, intval($values->number), intval($values->financial_reward), $values->financial_premium);

            $this->grid->reload();
            $this->getParent()->redrawControl('snipStaffCount');
        }
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $that = $this;
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModelForSchoolInNature($this->schoolInNature));

        // DATA

        // COLUMNS
        $grid->addColumnText('name', 'grid.school-in-nature.staff.name');
        $grid->addColumnText('description', 'grid.school-in-nature.staff.description');
        $grid->addColumnText('number', 'grid.school-in-nature.staff.number')
            ->setRenderer(static function (eStaff $staff) use ($that): Html {
                $render = new Html();
                $render->addHtml(Html::el('span')->setText(sprintf('%d / %d', $staff->getNumberOfLoggedUsers(), $staff->getNumber())));

                if ($staff->getNumberOfLoggedUsers() === 0) {
                    return $render;
                }

                foreach ($staff->getUsers() as $user) {
                    $render->addHtml(
                        Html::el('a', [
                            'class' => 'ajax text-nowrap text-danger d-block',
                            'href'  => $that->link('removeUser!', [
                                'sid' => $staff->getId(),
                                'uid' => $user->getId(),
                            ]),
                        ])->setText($user->getFullName())
                    );
                }

                return $render;
            })->setAlign('center');
        $grid->addColumnText('financial_reward', 'grid.school-in-nature.staff.financial-reward');
        $grid->addColumnText('financial_premium', 'grid.school-in-nature.staff.financial-premium');

        // FILTER
        $grid->addFilterText('name', 'grid.school-in-nature.staff.name');
        $grid->addFilterText('description', 'grid.school-in-nature.staff.description');
        $grid->addFilterText('number', 'grid.school-in-nature.staff.number');
        $grid->addFilterText('financial_reward', 'grid.school-in-nature.staff.financial-reward');
        $grid->addFilterText('financial_premium', 'grid.school-in-nature.staff.financial-premium');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::DELETE)) {
            $grid->addActionCallback('remove', 'grid.school-in-nature.staff.action.remove', [$this, 'gridStaffRemove'])
                ->setIcon('trash')
                ->setTitle('grid.school-in-nature.staff.action.remove')
                ->setConfirmation(new StringConfirmation('grid.school-in-nature.staff.action.remove %%s', 'name'))
                ->setClass('btn btn-xs btn-outline-danger ajax');
        }

        /* INLINE EDIT/ADD */
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addInlineEdit()->onControlAdd[]  = [$this, 'gridInlineEditCreate'];
            $grid->getInlineEdit()->onSetDefaults[] = [$this, 'gridInlineEditDefaults'];
            $grid->getInlineEdit()->onSubmit[]      = [$this, 'gridInlineEditSubmit'];

            $grid->addInlineAdd()
                ->setPositionTop()
                ->setText($this->translator->translate('grid.school-in-nature.staff.action.add'))
                ->onControlAdd[]              = [$this, 'gridInlineAddCreate'];
            $grid->getInlineAdd()->onSubmit[] = [$this, 'gridInlineAddSubmit'];
        }

        $this->grid = $grid;
        return $grid;
    }

    public function gridStaffRemove(string $id): void
    {
        $staff     = $this->facade->get(intval($id));
        $presenter = $this->getPresenterIfExists();

        $this->facade->remove($staff);

        if ($presenter !== null) {
            $presenter->flashMessage('grid.school-in-nature.staff.action.flash.remove.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }

    public function createComponentStaffAdd(): Form
    {
        // DATA
        [$dataStaff, $dataUser] = $this->prepareFormData();

        // FORM
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addSelect('staff', 'grid.school-in-nature.staff.add.staff', $dataStaff)
            ->setRequired('grid.school-in-nature.staff.add.staff.req')
            ->setTranslator(null)
            ->setPrompt(Constant::PROMTP);

        $form->addSelect('user', 'grid.school-in-nature.staff.add.user', $dataUser)
            ->setRequired('grid.school-in-nature.staff.add.user.req')
            ->setTranslator(null)
            ->setPrompt(Constant::PROMTP)
            ->setHtmlAttribute('data-chosen-placeholder-text', $this->translator->translate('grid.school-in-nature.staff.add.user.chosen-placeholder-text'))
            ->setHtmlAttribute('data-chosen-no-result-text', $this->translator->translate('grid.school-in-nature.staff.add.user.chosen-no-result-text'));

        // BUTTON
        $form->addSubmit('send', 'grid.school-in-nature.staff.add.send');

        // CALLBACK
        $form->onSuccess[] = [$this, 'processStaffAddOnSuccess'];

        return $form;
    }

    protected function prepareFormData(): array
    {
        $currentUsers = [];

        $dataStaff = [];
        /** @var eStaff $staff */
        foreach ($this->facade->getModelForSchoolInNature($this->schoolInNature)->getQuery()->getResult() as $staff) {
            foreach ($staff->getUsers() as $user) {
                $currentUsers[] = $user->getId();
            }

            if ($staff->getNumberOfLoggedUsers() < $staff->getNumber()) {
                $dataStaff[$staff->getId()] = $staff->getName();
            }
        }

        $dataUser = [];
        foreach ($this->facadeUser->getModelForGrid()->getQuery()->getResult() as $user) {
            if (! in_array($user->getId(), $currentUsers)) {
                $dataUser[$user->getId()] = sprintf('%s <%s>', $user->getFullName(), $user->getEmail());
            }
        }

        return [$dataStaff, $dataUser];
    }

    public function processStaffAddOnSuccess(Form $form, ArrayHash $values): void
    {
        $staff     = $this->facade->get($values->staff);
        $user      = $this->facadeUser->get($values->user);
        $presenter = $this->getPresenterIfExists();

        if ($this->facade->addUserToStaff($staff, $user)) {
            if ($presenter !== null) {
                $presenter->flashMessage('grid.school-in-nature.staff.add.flash.success.create', Flash::SUCCESS);
            }
            $this['grid']->reload();
        } elseif ($presenter !== null) {
            $presenter->flashMessage('grid.school-in-nature.staff.add.flash.danger.create', Flash::DANGER);
        }

        [$dataStaff, $dataUser] = $this->prepareFormData();
        $form['staff']->setItems($dataStaff);
        $form['user']->setItems($dataUser);

        $this->redrawControl('snipStaffAdd');
    }

    public function handleRemoveUser(int $sid, int $uid): void
    {
        $staff     = $this->facade->get($sid);
        $user      = $this->facadeUser->get($uid);
        $presenter = $this->getPresenterIfExists();

        $this->facade->removeUserFromStaff($staff, $user);

        if ($presenter !== null) {
            $presenter->flashMessage('grid.school-in-nature.staff.action.flash.remove-user.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
        $this->redrawControl('snipStaffAdd');
    }
}
