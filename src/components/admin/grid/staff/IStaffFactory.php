<?php

declare(strict_types=1);

namespace Skadmin\SchoolInNature\Components\Admin;

use Skadmin\SchoolInNature\Doctrine\SchoolInNature\SchoolInNature;

/**
 * Interface IOverviewFactory
 */
interface IStaffFactory
{
    public function create(SchoolInNature $schoolInNature) : Staff;
}
