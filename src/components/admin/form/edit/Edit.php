<?php

declare(strict_types=1);

namespace Skadmin\SchoolInNature\Components\Admin;

use App\Components\Form\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;
use Skadmin\File\Components\Admin\CreateComponentFormFile;
use Skadmin\File\Components\Admin\FileDownloadByFacade;
use Skadmin\File\Components\Admin\FileRemoveByFacade;
use Skadmin\File\Components\Admin\FormFile;
use Skadmin\File\Components\Admin\IFormFileFactory;
use Skadmin\FileStorage\FileStorage;
use Skadmin\SchoolInNature\BaseControl;
use Skadmin\SchoolInNature\Doctrine\SchoolInNature\SchoolInNature;
use Skadmin\SchoolInNature\Doctrine\SchoolInNature\SchoolInNatureFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function explode;
use function intval;
use function is_bool;

/**
 * Class Edit
 */
class Edit extends FormWithUserControl
{
    use APackageControl;
    use CreateComponentFormFile;
    use FileDownloadByFacade {
        CreateComponentFormFile::checkFileObject insteadof FileDownloadByFacade;
    }
    use FileRemoveByFacade {
        CreateComponentFormFile::checkFileObject insteadof FileRemoveByFacade;
    }

    /** @var IStaffFactory */
    public $iStaffFactory;

    /** @var LoaderFactory */
    public $webLoader;

    /** @var SchoolInNatureFacade */
    private $facade;

    /** @var SchoolInNature */
    private $schoolInNature;

    /** @var ImageStorage */
    private $imageStorage;

    /** @var FormFile */
    private $formFile;

    public function __construct(?int $id, SchoolInNatureFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, IStaffFactory $iStaffFactory, ImageStorage $imageStorage, IFormFileFactory $iFormFileFactory, FileStorage $fileStorage)
    {
        parent::__construct($translator, $user);
        $this->facade       = $facade;
        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->schoolInNature = $this->facade->get($id);
        $this->fileObject     = $this->schoolInNature;
        $this->fileStorage    = $fileStorage;

        $this->iStaffFactory = $iStaffFactory;

        if (! $this->schoolInNature->isLoaded()) {
            return;
        }

        $this->formFile = $iFormFileFactory->create();
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->schoolInNature->isLoaded()) {
            return new SimpleTranslation('school-in-nature.edit.title - %s', $this->schoolInNature->getName());
        }

        return 'school-in-nature.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss() : array
    {
        $css = [
            $this->webLoader->createCssLoader('daterangePicker'),
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];

        if ($this->schoolInNature->isLoaded()) {
            foreach ($this->formFile->getCss() as $wl) {
                $css[] = $wl;
            }
        }

        return $css;
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        $js = [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('moment'),
            $this->webLoader->createJavaScriptLoader('daterangePicker'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];

        if ($this->schoolInNature->isLoaded()) {
            foreach ($this->formFile->getJs() as $wl) {
                $js[] = $wl;
            }
        }

        return $js;
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        /**
         * @var DateTime $termFrom
         * @var DateTime $termTo
         */
        [$termFrom, $termTo] = Arrays::map(explode(' - ', $values->term), static function (string $date) : DateTime {
            $date = DateTime::createFromFormat('d.m.Y', $date);
            return is_bool($date) ? new DateTime() : $date;
        });

        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE);

        if ($this->schoolInNature->isLoaded()) {
            if ($identifier !== null && $this->schoolInNature->getImagePreview() !== null) {
                $this->imageStorage->delete($this->schoolInNature->getImagePreview());
            }

            $schoolInNature = $this->facade->update(
                $this->schoolInNature->getId(),
                $values->name,
                intval($values->number_of_children),
                $values->content,
                $termFrom,
                $termTo,
                $values->place_name,
                $values->place_address,
                $values->place_gps_lat,
                $values->place_gps_lng,
                $identifier
            );
            $this->onFlashmessage('form.school-in-nature.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $schoolInNature = $this->facade->create(
                $values->name,
                intval($values->number_of_children),
                $values->content,
                $termFrom,
                $termTo,
                $values->place_name,
                $values->place_address,
                $values->place_gps_lat,
                $values->place_gps_lng,
                $identifier
            );
            $this->onFlashmessage('form.school-in-nature.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $schoolInNature->getId(),
        ]);
    }

    public function processOnBack() : void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render() : void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->schoolInNature = $this->schoolInNature;
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.school-in-nature.edit.name')
            ->setRequired('form.school-in-nature.edit.name.req');
        $form->addTextArea('content', 'form.school-in-nature.edit.content');

        // SchoolInNature
        $form->addText('number_of_children', 'form.school-in-nature.edit.number-of-children')
            ->setRequired('form.school-in-nature.edit.number-of-children.req')
            ->setHtmlType('number')
            ->setHtmlAttribute('min', 0);
        $form->addText('term', 'form.school-in-nature.edit.term')
            ->setRequired('form.school-in-nature.edit.term.req')
            ->setHtmlAttribute('data-daterange');
        $form->addImageWithRFM('image_preview', 'form.school-in-nature.edit.image-preview');

        // PLACE
        $form->addText('place_name', 'form.school-in-nature.edit.place-name');
        $form->addText('place_address', 'form.school-in-nature.edit.place-address');
        $form->addText('place_gps_lat', 'form.school-in-nature.edit.place-gps-lat');
        $form->addText('place_gps_lng', 'form.school-in-nature.edit.place-gps-lng');

        // BUTTON
        $form->addSubmit('send', 'form.school-in-nature.edit.send');
        $form->addSubmit('send_back', 'form.school-in-nature.edit.send-back');
        $form->addSubmit('back', 'form.school-in-nature.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        if (! $this->schoolInNature->isLoaded()) {
            return [];
        }

        return [
            'name'               => $this->schoolInNature->getName(),
            'content'            => $this->schoolInNature->getContent(),
            'number_of_children' => $this->schoolInNature->getNumberOfChildren(),
            'term'               => $this->schoolInNature->getTermFromTo(),
            'place_name'         => $this->schoolInNature->getPlaceName(),
            'place_address'      => $this->schoolInNature->getPlaceAddress(),
            'place_gps_lat'      => $this->schoolInNature->getPlaceGpsLat(),
            'place_gps_lng'      => $this->schoolInNature->getPlaceGpsLng(),
        ];
    }

    protected function createComponentStaff() : Staff
    {
        return $this->iStaffFactory->create($this->schoolInNature);
    }
}
