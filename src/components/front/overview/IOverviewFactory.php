<?php

declare(strict_types=1);

namespace Skadmin\SchoolInNature\Components\Front;

/**
 * Interface IOverviewFactory
 */
interface IOverviewFactory
{
    public function create() : Overview;
}
