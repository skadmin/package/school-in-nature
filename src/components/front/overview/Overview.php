<?php

declare(strict_types=1);

namespace Skadmin\SchoolInNature\Components\Front;

use App\Components\Grid\TemplateControl;
use App\Model\System\APackageControl;
use SkadminUtils\ImageStorage\ImageStorage;
use Skadmin\SchoolInNature\BaseControl;
use Skadmin\SchoolInNature\Doctrine\SchoolInNature\SchoolInNatureFacade;
use Skadmin\Translator\Translator;

/**
 * Class Detail
 */
class Overview extends TemplateControl
{
    use APackageControl;

    /** @var SchoolInNatureFacade */
    private $facade;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(SchoolInNatureFacade $facade, Translator $translator, ImageStorage $imageStorage)
    {
        parent::__construct($translator);
        $this->facade       = $facade;
        $this->imageStorage = $imageStorage;
    }

    public function getTitle(): string
    {
        return 'school-in-nature.front.overview.title';
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');

        $template->schoolsInNature    = $this->facade->getSchoolsInNatureInFuture();
        $template->oldSchoolsInNature = $this->facade->getSchoolsInNatureInPast();
        $template->package            = new BaseControl();

        $template->render();
    }
}
