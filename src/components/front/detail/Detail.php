<?php

declare(strict_types=1);

namespace Skadmin\SchoolInNature\Components\Front;

use App\Components\Grid\TemplateControl;
use App\Model\Doctrine\User\User;
use App\Model\Doctrine\User\UserFacade;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\Security\User as LoggedUser;
use Skadmin\File\Components\Admin\FileDownloadByFacade;
use Skadmin\FileStorage\FileStorage;
use Skadmin\SchoolInNature\BaseControl;
use Skadmin\SchoolInNature\Doctrine\SchoolInNature\SchoolInNature;
use Skadmin\SchoolInNature\Doctrine\SchoolInNature\SchoolInNatureFacade;
use Skadmin\SchoolInNature\Doctrine\Staff\StaffFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;

/**
 * Class Detail
 */
class Detail extends TemplateControl
{
    use APackageControl;
    use FileDownloadByFacade;

    /** @var SchoolInNatureFacade */
    private $facade;

    /** @var StaffFacade */
    private $facadeStaff;

    /** @var UserFacade */
    private $facadeUser;

    /** @var User */
    private $user;

    /** @var SchoolInNature */
    private $schoolInNature;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(int $id, SchoolInNatureFacade $facade, StaffFacade $facadeStaff, UserFacade $facadeUser, LoggedUser $user, Translator $translator, ImageStorage $imageStorage, FileStorage $fileStorage)
    {
        parent::__construct($translator);
        $this->facade       = $facade;
        $this->facadeStaff  = $facadeStaff;
        $this->facadeUser   = $facadeUser;
        $this->imageStorage = $imageStorage;

        $this->user           = $this->facadeUser->get($user->getId());
        $this->schoolInNature = $this->facade->get($id);
        $this->fileObject     = $this->schoolInNature;
        $this->fileStorage    = $fileStorage;
    }

    public function getTitle() : SimpleTranslation
    {
        return new SimpleTranslation('school-in-nature.front.detail.title - %s', $this->schoolInNature->getName());
    }

    public function render() : void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/detail.latte');

        $template->schoolInNature = $this->schoolInNature;
        $template->package        = new BaseControl();

        $template->loggedUser = $this->user;

        $template->render();
    }

    public function handleLogin(int $id) : void
    {
        if ($this->facadeStaff->addUserToStaff($id, $this->user)) {
            $this->onFlashmessage('school-in-nature.front.detail.flash.success.login', Flash::SUCCESS);
        } else {
            $this->onFlashmessage('school-in-nature.front.detail.flash.danger.login', Flash::DANGER);
        }

        $this->redrawDetail();
    }

    private function redrawDetail() : void
    {
        $this->redrawControl('snipStaff');
        $this->redrawControl('snipNumberOfStaff');
    }

    public function handleLogout(int $id) : void
    {
        $this->facadeStaff->removeUserFromStaff($id, $this->user);
        $this->onFlashmessage('school-in-nature.front.detail.flash.success.logout', Flash::SUCCESS);
        $this->redrawDetail();
    }
}
