<?php

declare(strict_types=1);

namespace Skadmin\SchoolInNature;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'school-in-nature';
    public const DIR_IMAGE = 'school-in-nature';

    public function getMenu() : ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-school']),
            'items'   => ['overview'],
        ]);
    }
}
