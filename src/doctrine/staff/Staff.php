<?php

declare(strict_types=1);

namespace Skadmin\SchoolInNature\Doctrine\Staff;

use SkadminUtils\DoctrineTraits\Entity;
use App\Model\Doctrine\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Skadmin\SchoolInNature\Doctrine\SchoolInNature\SchoolInNature;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'school_in_nature_staff')]
#[ORM\HasLifecycleCallbacks]
class Staff
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Description;
    use Entity\Number;
    use Entity\FinancialReward;
    use Entity\FinancialPremium;

    #[ORM\ManyToOne(targetEntity: SchoolInNature::class, inversedBy: 'staff')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private SchoolInNature $schoolInNature;

    /** @var ArrayCollection<int, User> */
    #[ORM\ManyToMany(targetEntity: User::class)]
    #[ORM\JoinTable(name: 'school_in_nature_staff_user')]
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function update(SchoolInNature $schoolInNature, string $name, string $description, int $number, int $financialReward, string $financialPremium) : void
    {
        $this->schoolInNature = $schoolInNature;
        $this->name           = $name;
        $this->description    = $description;
        $this->number         = $number;

        $this->financialReward  = $financialReward;
        $this->financialPremium = $financialPremium;
    }

    public function addUser(User $user) : bool
    {
        if ($this->isUserLoggedInSchoolInNature($user)) {
            return false;
        }

        if ($this->getNumberOfLoggedUsers() >= $this->getNumber()) {
            return false;
        }

        $this->users->add($user);
        return true;
    }

    public function isUserLoggedInSchoolInNature(User $user) : bool
    {
        /** @var Staff $staff */
        foreach ($this->getSchoolInNature()->getStaff() as $staff) {
            if ($staff->getUsers()->contains($user)) {
                return true;
            }
        }

        return false;
    }

    public function getSchoolInNature() : SchoolInNature
    {
        return $this->schoolInNature;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    public function getNumberOfLoggedUsers() : int
    {
        return $this->users->count();
    }

    public function removeUser(User $user) : void
    {
        $this->users->removeElement($user);
    }

    public function isUserLogged(User $user) : bool
    {
        return $this->users->contains($user);
    }
}
