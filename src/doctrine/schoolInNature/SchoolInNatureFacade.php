<?php

declare(strict_types=1);

namespace Skadmin\SchoolInNature\Doctrine\SchoolInNature;

use SkadminUtils\DoctrineTraits\Facade;
use DateTimeInterface;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\File\Doctrine\File\File;

/**
 * Class SchoolInNatureFacade
 */
final class SchoolInNatureFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = SchoolInNature::class;
    }

    public function create(string $name, int $numberOfChildren, string $content, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placName, string $placeAddress, string $placeGpsLat, string $placeGpsLng, ?string $imagePreview): SchoolInNature
    {
        return $this->update(null, $name, $numberOfChildren, $content, $termFrom, $termTo, $placName, $placeAddress, $placeGpsLat, $placeGpsLng, $imagePreview);
    }

    public function update(?int $id, string $name, int $numberOfChildren, string $content, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $placName, string $placeAddress, string $placeGpsLat, string $placeGpsLng, ?string $imagePreview): SchoolInNature
    {
        $article = $this->get($id);
        $article->update($name, $numberOfChildren, $content, $termFrom, $termTo, $placName, $placeAddress, $placeGpsLat, $placeGpsLng, $imagePreview);

        $this->em->persist($article);
        $this->em->flush();

        return $article;
    }

    public function get(?int $id = null): SchoolInNature
    {
        if ($id === null) {
            return new SchoolInNature();
        }

        $schoolInNature = parent::get($id);

        if ($schoolInNature === null) {
            return new SchoolInNature();
        }

        return $schoolInNature;
    }

    /**
     * @return SchoolInNature[]
     */
    public function getSchoolsInNatureInFuture(?int $limit = null): array
    {
        $qb = $this->getModel();
        $qb->where('a.termFrom >= :termFrom')
            ->setParameter('termFrom', (new DateTime())->setTime(0, 0, 0))
            ->orderBy('a.termFrom');

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return SchoolInNature[]
     */
    public function getSchoolsInNatureInPast(?int $limit = null): array
    {
        $qb = $this->getModel();
        $qb->where('a.termTo < :termTo')
            ->setParameter('termTo', (new DateTime())->setTime(0, 0, 0))
            ->orderBy('a.termTo', 'DESC');

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    public function addFile(SchoolInNature $schoolInNature, File $file): void
    {
        $schoolInNature->addFile($file);
        $this->em->flush();
    }

    public function getFileByHash(SchoolInNature $schoolInNature, string $hash): ?File
    {
        return $schoolInNature->getFileByHash($hash);
    }

    public function removeFileByHash(SchoolInNature $schoolInNature, string $hash): void
    {
        $schoolInNature->removeFileByHash($hash);
        $this->em->flush();
    }
}
