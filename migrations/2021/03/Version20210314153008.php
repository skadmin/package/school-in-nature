<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210314153008 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'school-in-nature.overview', 'hash' => 'abdfd7653af1dd1857ffecfd2bd75d1d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Školy v přírodě', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.school-in-nature.title', 'hash' => '024fbff9a7ee78d6467f2db7ebe3acdd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Školy v přírodě', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.school-in-nature.description', 'hash' => 'a8c2920f59ac76a971fb466d1fe6398a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat školy v přírodě', 'plural1' => '', 'plural2' => ''],
            ['original' => 'school-in-nature.overview.title', 'hash' => '8efac646393f27c9ebf54e9d54fe2eea', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Školy v přírodě|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.school-in-nature.overview.action.new', 'hash' => 'a05f80182666a8d9016131b96c25e3ee', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit školu v přírodě', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.school-in-nature.overview.name', 'hash' => 'fed8f80ad1b8c4ea4138ed94da29e000', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.school-in-nature.overview.place-name', 'hash' => 'a57fe4164ae2e01de9ed47bd92cf8c87', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Místo konání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.school-in-nature.overview.term', 'hash' => 'd4a66eb32c2e5c394d0d1f583ed13649', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.school-in-nature.overview.number-of-children', 'hash' => '9cc3c3eeaafd1ff39bdada4ac561e482', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet dětí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.school-in-nature.overview.number-of-staff', 'hash' => '6da5bd3437d5d5cd170550e9d5847903', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet personálu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.school-in-nature.overview.total-number-of-people', 'hash' => '24d4a799f8e74565e7c25dda5547bdba', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Celkový počet', 'plural1' => '', 'plural2' => ''],
            ['original' => 'school-in-nature.edit.title', 'hash' => '91fed8c871ada770b80ccfafedf3421d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení školy v přírodě', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.name', 'hash' => '97cb2ba460a68cb50922311b099dd8e8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.name.req', 'hash' => 'd349b38d03b87ae2053f445090a7c4a8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.number-of-children', 'hash' => '81a2c2fa7fc61f22f710caea2bfd36dc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet dětí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.number-of-children.req', 'hash' => 'da760522b3e64a27c0389505980f85a3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím počet dětí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.term', 'hash' => '402285619a0694e9aac9a2c7fbc597e8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.term.req', 'hash' => '39fe3384701095f561fa1da7463c3d8f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.place-name', 'hash' => '7707d2466938e4eeb5b56bd97761d454', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název místa konání', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.place-address', 'hash' => '56cee84a57be62e2c9d43d31aabccaf2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Adresa', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.place-gps-lat', 'hash' => '5e4b7c1684c8833520d4b02ba9d24bd6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'GPS šířka', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.place-gps-lng', 'hash' => '41276f3b54f69ada971ffd3ac3e659f9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'GPS výška', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.image-preview', 'hash' => 'fbfc60ebaa86c8c015f01cab5542ec18', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled školy v přírodě', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.image-preview.rule-image', 'hash' => '2a4b473fcad71924aceb6c572cb26bb4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.content', 'hash' => 'fc406fb1455f280572d727775669583c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.send', 'hash' => '3f72f3db94699fe7fb00e658f96a95f7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.send-back', 'hash' => 'f4e93a48cdf3d06e37775f114b0400b2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.back', 'hash' => '2a105a42967c3d0dac01c2e04ac25696', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'school-in-nature.edit.title - %s', 'hash' => 'bdbfbc80bdc850fb76f5772234a97422', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace školy v přírodě', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.flash.success.create', 'hash' => '44ba9a3908f028f7e399db89a2591f11', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Škola v přírodě byla úspěšně založena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.school-in-nature.staff.action.add', 'hash' => '74b63814f33dd66fe49a68de8ade40b9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přidat personál', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.school-in-nature.staff.name', 'hash' => 'e11f99c4056b79ecfd05cb2d5ac90cd4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pozice', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.school-in-nature.staff.description', 'hash' => '19b8992d8edb2b7776d8bbb2fca73294', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.school-in-nature.staff.number', 'hash' => '452e3c503d10b8e67204760661c9450a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.school-in-nature.staff.financial-reward', 'hash' => '4d6e2e22d8c50e94ee1d59b2b8a61594', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odměna', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.school-in-nature.staff.financial-premium', 'hash' => 'b7a8c06a2d3f85249ae9facb9f7559da', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Prémie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.flash.success.update', 'hash' => 'e8edf11816a432c8364b483aa9ca8dd3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Škola v přírodě byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.school-in-nature.overview.action.edit', 'hash' => '1cbfbba7f63eb9ba414e76830b0b83f4', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.school-in-nature.overview.action.detail', 'hash' => '51db8d703ae0ee624e31d266c98d1222', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.file.name', 'hash' => '79651e9ee004bb03d19112ebd4e27182', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.file.size', 'hash' => '16c537b0c72be2e4b055128eff3f699a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Velikost', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.file.last-update-at', 'hash' => '50da68cee369029dbe843568b9e4e38c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Upraveno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.file.action.download', 'hash' => '0cd6a8510b9767603cd8e4956e10384b', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.file.remove.confirm', 'hash' => '07c5e0248f54f7a320acec4384876e69', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete odebrat dokument?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.school-in-nature.edit.file.action.remove', 'hash' => '0ed8f9d437eed0fa7a295e5b840356f5', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.title - %s', 'hash' => '060ed87cb4427f466ffd978ca5ee09da', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Detail školy v přírodě', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.term', 'hash' => 'e35c08633b757b31072474a1038deadd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Termín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.number-of-children', 'hash' => 'b9ded469c330546a081ce2596e26b66f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet dětí', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.number-of-staff', 'hash' => '20b98571db8f996f4bbf1efdae6071b4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet personálu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.total-number-of-people', 'hash' => '28ec7d037d0f0ecbb47a6036000d007f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Celkový počet', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.staff.title', 'hash' => '20718104e7dec83087c52571937ee9c7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Personál', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.staff.name', 'hash' => '7218f3dc248477cca41ed833882f8d3a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pozice', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.staff.financial-reward', 'hash' => '393988cd2cf3dce02a33059bd4b44cef', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odměna', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.staff.financial-premium', 'hash' => '74fa2eff8ec1c0deac69b9160ac0af39', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Prémie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.staff.logged', 'hash' => 'd76cff20e97f3ae7524819287ab8f368', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsazenost', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.staff.log-in', 'hash' => '29bf2acb465c4262021a47bfe1178a5e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přihlásit se', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.content.title', 'hash' => 'cb37c96d098bb6b56c53167eedf2c8e8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.files.title', 'hash' => 'f8a1543cc2c54ed7ad3b11c014d34766', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Dokumenty', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.file.name', 'hash' => 'a461ed34dc484042d7350e6a1e8beef9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.file.size', 'hash' => 'ee0e09a9168168e70a3376d3c33f5a5e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Velikost', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.file.last-update-at', 'hash' => 'f7da6b59d89ecfb147fe57634b7f8836', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Upraveno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.file.action.download', 'hash' => '888c7f413afbaf00e8c9dee29d5a52d2', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.flash.success.login', 'hash' => '4a5646765f016d54f8b960d8facf169c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Úspěšně jste se přihlásili.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.staff.log-out-user.confirm', 'hash' => '3d381fbc633fa49d43b7eeec13594c53', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete odebrat účastníka?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.staff.log-out.confirm', 'hash' => '8ef753b90d31e4a72c24d53a781ed2dc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu se chcete odhlásit?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.staff.log-out', 'hash' => 'ba32aaf8e173c69032753e5360880ae1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odhlásit se', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.flash.success.logout', 'hash' => '75fd02faaf399356f0e7ff783f7e086b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Úspěšně jste se odhlásili.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.flash.success.logout-user', 'hash' => 'd77f632380f814950daca03ca71461f0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Úspěšně jste odhlásili účastníka.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'extract.school-in-nature.detail.place', 'hash' => '34091df9ff07c36831629b50880ae481', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Adresa', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
