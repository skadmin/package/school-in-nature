<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191119103331 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE school_in_nature_file (school_in_nature_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_C9A9249A92637F56 (school_in_nature_id), INDEX IDX_C9A9249A93CB796C (file_id), PRIMARY KEY(school_in_nature_id, file_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE school_in_nature_file ADD CONSTRAINT FK_C9A9249A92637F56 FOREIGN KEY (school_in_nature_id) REFERENCES school_in_nature (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE school_in_nature_file ADD CONSTRAINT FK_C9A9249A93CB796C FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE school_in_nature_file');
    }
}
