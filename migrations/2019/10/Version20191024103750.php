<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191024103750 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE school_in_nature (id INT AUTO_INCREMENT NOT NULL, number_of_children INT NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, term_from DATETIME NOT NULL, term_to DATETIME NOT NULL, place_name VARCHAR(255) NOT NULL, place_address VARCHAR(255) NOT NULL, place_gps_lat VARCHAR(255) NOT NULL, place_gps_lng VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE school_in_nature_staff (id INT AUTO_INCREMENT NOT NULL, school_in_nature_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, number INT NOT NULL, financial_reward INT NOT NULL, financial_premium VARCHAR(255) NOT NULL, INDEX IDX_4F46AFBE92637F56 (school_in_nature_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE school_in_nature_staff_user (staff_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_69BFBBCCD4D57CD (staff_id), INDEX IDX_69BFBBCCA76ED395 (user_id), PRIMARY KEY(staff_id, user_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE school_in_nature_staff ADD CONSTRAINT FK_4F46AFBE92637F56 FOREIGN KEY (school_in_nature_id) REFERENCES school_in_nature (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE school_in_nature_staff_user ADD CONSTRAINT FK_69BFBBCCD4D57CD FOREIGN KEY (staff_id) REFERENCES school_in_nature_staff (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE school_in_nature_staff_user ADD CONSTRAINT FK_69BFBBCCA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');

        /* DATA */
        $resources = [
            [
                'name'        => 'school-in-nature',
                'title'       => 'role-resource.school-in-nature.title',
                'description' => 'role-resource.school-in-nature.description',
            ],
        ];

        foreach ($resources as $resource) {
            $this->addSql('INSERT INTO core_role_resource (name, title, description) VALUES (:name, :title, :description)', $resource);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE school_in_nature_staff DROP FOREIGN KEY FK_4F46AFBE92637F56');
        $this->addSql('ALTER TABLE school_in_nature_staff_user DROP FOREIGN KEY FK_69BFBBCCD4D57CD');
        $this->addSql('DROP TABLE school_in_nature');
        $this->addSql('DROP TABLE school_in_nature_staff');
        $this->addSql('DROP TABLE school_in_nature_staff_user');
    }
}
